<?php

namespace Restful;

use Restful\Exception\UnauthorizedException;

class Request {

    public $method = "GET",
           $format = "json",
           $uri,
           $credentials;

    private $server;

    public function __construct(Server $server = null)
    {
        $this->server = $server;

        $this->parseUrl();
        $this->setMethod();
        $this->setCredentials();
    }

    private function parseUrl()
    {
        $url = str_ireplace($this->server->basePath . "/" . $this->server->version, "", $_SERVER["REQUEST_URI"]);
        preg_match("|/(?<uri>[^.?]*)\.?(?<format>\w+)?|i", $url, $result);


        if(isset($result["format"])){
            $this->setFormat($result["format"]);
        } else if(isset($_GET["format"])){
            $this->setFormat($_GET["format"]);
        }

        $this->uri = $result["uri"];
    }

    public function setMethod()
    {
        $method = $_SERVER['REQUEST_METHOD'];

        if(isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'])){
            $override = $_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'];
        }elseif(isset($_GET["method"])){
            $override = $_GET["method"];
        }

        if(isset($override)){
            $override = strtoupper($override);
            if(array_search($override, $this->server->methods)) {
                $this->method = $override;
            }
        }

        $this->method = $method;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setFormat($format)
    {
        if(array_search($format, $this->server->formats)){
            $this->format = $format;
        }
    }

    public function getFormat()
    {
        return $this->format;
    }

    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function setCredentials()
    {
        if (isset($_SERVER['PHP_AUTH_USER'])) {
            $this->credentials = [$_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']];
        } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            if (strpos(strtolower($_SERVER['HTTP_AUTHORIZATION']),'basic')===0) {
                $this->credentials = explode(':', base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
            }
        } else {
            if(method_exists($this->server, 'log')){
                call_user_func([$this->server, 'log'], 401, $this->method, $this->uri);
            }
            $this->server->getResponse()->error(new UnauthorizedException);
        }
    }

    public function getCredentials()
    {
        return $this->credentials;
    }

}