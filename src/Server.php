<?php

namespace Restful;

use Restful\Exception\UnauthorizedException;
use Restful\Exception\NotFoundException;

class Server {

    public $methods = ["POST", "GET", "PUT", "DELETE"],
           $formats = ["json", "xml"];

    public $cacheDir,
           $basePath,
           $version;

    private $routes;

    public function __construct()
    {
        $this->response = new Response($this);
        $this->request  = new Request($this);
    }

    public function getBasePath()
    {
        return $this->basePath;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function post($uri, $callback)
    {
        return $this->addRoute("POST", $uri, $callback);
    }

    public function get($uri, $callback)
    {
        return $this->addRoute("GET", $uri, $callback);
    }

    public function put($uri, $callback)
    {
        return $this->addRoute("PUT", $uri, $callback);
    }

    public function delete($uri, $callback)
    {
        return $this->addRoute("DELETE", $uri, $callback);
    }

    private function addRoute($method, $uri, $callback)
    {
        $this->routes[$method][$uri] = $callback;

        return $this;
    }

    public function run()
    {
        $method = $this->request->method;
        $uri = $this->request->uri;

        if(!array_key_exists($method, $this->routes)){
            if(method_exists($this, 'log')){
                call_user_func([$this, 'log'], 404, $this->request->method, $this->request->uri);
            }
            $this->response->error(new NotFoundException);
        }

        if(isset($this->routes[$method][$uri]) && $callable = $this->routes[$method][$uri]){
            $this->call($callable);
        } else {
            foreach ($this->routes[$method] as $route=>$callback) {
                preg_match("|$route|is", $uri, $result);
                if(count($result) > 0){
                    array_shift($result);
                    $this->call($callback, $result);
                }
            }
        }
    }

    private function call($callable, $arguments = [])
    {
        if(method_exists($this, 'auth')){
            if(false === call_user_func_array([$this, 'auth'], $this->request->credentials)){
                if(method_exists($this, 'log')){
                    call_user_func([$this, 'log'], 401, $this->request->method, $this->request->uri);
                }

                $this->response->error(new UnauthorizedException);
            }
        }

        if(method_exists($this, 'log')){
            call_user_func([$this, 'log'], 200, $this->request->method, $this->request->uri);
        }

        if(is_string($callable)) {
            $callable = explode("::", $callable);
        }

        $this->response->create(200, call_user_func_array($callable, $arguments));
    }

}