<?php

namespace Restful\Exception;

class UnauthorizedException extends \Exception {
    protected $code = 401;
    protected $message = 'Invalid authentication credentials';
}