<?php

namespace Restful;


class Response {

    public
        $code,
        $body,
        $headers = array('content-type' => 'application/json');

    private $server;

    public function __construct(Server $server = null)
    {
        $this->server = $server;
    }

    public function error(\Exception $exception)
    {
        $body = array(
            "message" => $exception->getMessage(),
        );
        $this->create($exception->getCode(), $body);
        throw new $exception;
    }

    public function create($code, Array $body)
    {
        $response = array(
            "status" => $code,
            "results" => $body,
        );
        foreach ($this->headers as $name => $value) {
            header($name.': '.$value, true, $code);
        }

        echo json_encode($response);
    }
}