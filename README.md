# RESTful API Server

Installation
------------

- `git clone https://bitbucket.org/sve/restful-server.git`
- `cd restful-server && composer update`
- `mysql -uroot -ptoor rest < schema.sql`
- Configure settings in `public/db.php`
- `php -S localhost:8080 -t public/`
- Start the engines by executing `curl -vH "Authorization: Basic cm9vdDp0b29y" localhost:8080/api/v1/users/1/email` or simply `curl -vu root:toor localhost:8080/api/v1/users/3/balances`