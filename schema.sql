DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp default '0000-00-00 00:00:00',
  `updated` timestamp default now() on update now(),
  `name` varchar(64) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL,
  `email` varchar(64) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `balance_usd` decimal(15,2) NOT NULL DEFAULT '0.00',
  `balance_gbp` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp default now(),
  `status` smallint NOT NULL,
  `method` varchar(10) DEFAULT NULL,
  `uri` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `users` (`created`, `name`, `password`, `email`, `active`, `balance_usd`, `balance_gbp`)
VALUES
('2015-05-12 18:09:22', 'User1', '90b37519842cf52192c5324b666c998f', 'first@mail.com', 1, 135.34, 0.00),
('2015-05-12 18:09:22', 'User2', '93c37afa203762995491707d5cfafd43', 'second@mail.com', 0, 0, 0),
('2015-05-12 18:09:22', 'User3', 'edd942475f01acaf04a97e9976bacf0c', 'third@mail.com', 1, 15.50, 20);