<?php

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '123');
define('DB_NAME', 'test');

class db
{
    private $link, $query, $result;
    private static $instance;

    public static function instance() {
        if(!self::$instance instanceof self) {
            self::$instance = new self;
            self::$instance->connect();
        }
        return self::$instance;
    }

    public function connect()
    {
        try {
            $this->link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
            $this->link->set_charset("utf8");
        } catch (Exception $e) {
            die('Unable to connect to database');
        }
    }

    public function filter($data)
    {
        if (!is_array($data)) {
            $data = $this->link->real_escape_string($data);
            $data = trim(htmlentities($data, ENT_QUOTES, 'UTF-8', false));
        } else {
            $data = array_map(array($this, 'filter'), $data);
        }
        return $data;
    }

    public function select($fields = "*")
    {
        if(is_array($fields)){
            $fields = implode( ', ', $fields );
        }
        $this->query = "SELECT ${fields} ";
        return $this;
    }

    public function insert($table, $data)
    {
        $values = "";
        $fields = array_keys($data);
        $this->query = "INSERT INTO `${table}` (`" . implode("`,`", $fields) . "`) ";
        foreach($data as $value){
            if(is_numeric($value)){
                $values .= "$value,";
            }elseif(is_null($value)){
                $values .= "null,";
            }else{
                $values .= "'$value',";
            }
        }
        $this->query .= "VALUES (" . rtrim($values, ",") . ")";
        return $this;
    }

    public function from($table)
    {
        $this->query .= "FROM ${table} ";
        return $this;
    }

    public function where($prop, $value)
    {
        $this->query .= "WHERE `${prop}` = '${value}'";
        return $this;
    }

    public function andWhere($prop, $value)
    {
        $this->query .= "AND `${prop}` = '${value}'";
        return $this;
    }

    public function orWhere($prop, $value)
    {
        $this->query .= "OR `${prop}` = '${value}'";
        return $this;
    }

    public function execute()
    {
        $this->result = $this->link->query($this->query);
        return $this;
    }

    public function fetch()
    {
        return $this->result->fetch_array(MYSQLI_ASSOC);
    }

    public function lastid()
    {
        return $this->link->insert_id;
    }

    public function __destruct()
    {
        if ($this->link) {
            $this->link->close();
        }
    }
}