<?php

require __DIR__ . '/../vendor/autoload.php';
require 'db.php';

use \Restful\Exception\NotFoundException;
use \Restful\Exception\UnauthorizedException;


class API extends Restful\Server {

    public $basePath = "api",
           $version = "v1";

    public function auth($username, $password)
    {
        return ($username == "root" && $password == "toor");
    }

    public function log($status, $method, $uri)
    {
        $data = array(
            "status" => $status,
            "method" => $method,
            "uri" => $uri,
        );

        db::instance()
            ->insert("logs", $data)
            ->execute();
    }

}

class UserAPI {

    public function getEmail($id)
    {
        $result = db::instance()
            ->select("email")
            ->from("users")
            ->where("id", $id)
            ->execute()
            ->fetch();

        return ["email" => $result["email"]];
    }

}

try {
    (new API)
        ->get("/users/(\d+)/email", "UserAPI::getEmail")
        ->get("/users/(\d+)/balances", function($id) {
            $result = db::instance()
                ->select(["balance_usd", "balance_gbp"])
                ->from("users")
                ->where("id", $id)
                ->execute()
                ->fetch();

            return ["usd" => $result["balance_usd"], "gbp" => $result["balance_gbp"]];
        })
        ->run();
} catch (NotFoundException $e) {

} catch (UnauthorizedException $e) {

}